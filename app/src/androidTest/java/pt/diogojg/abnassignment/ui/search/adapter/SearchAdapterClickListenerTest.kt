package pt.diogojg.abnassignment.ui.search.adapter

import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import pt.diogojg.abnassignment.ui.detail.VenueDetailActivity
import pt.diogojg.abnassignment.ui.search.SearchActivity
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue


class SearchAdapterClickListenerTest {
    lateinit var searchAdapterClickListener: SearchAdapterClickListener

    @get:Rule
    var activityRule: IntentsTestRule<SearchActivity> = IntentsTestRule(SearchActivity::class.java)

    @Before
    fun setUp() {
        searchAdapterClickListener = SearchAdapterClickListener(activityRule.activity)
    }

    @Test
    fun testOnCLickListener_shouldStartVenueDetailActivity() {
        val venue = Venue("1", "A", Location("S", "C", "C"))
        searchAdapterClickListener.onCLickListener(venue)
        intended(hasComponent(VenueDetailActivity::class.java.name))
    }
}