package pt.diogojg.abnassignment.ui.search.adapter

import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.android.synthetic.main.view_venue_item.view.*
import org.hamcrest.Matchers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import pt.diogojg.abnassignment.ui.search.view.venueitem.VenueItemView
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue

class VenueViewHolderTest {
    private lateinit var vh: VenueViewHolder

    @Before
    fun setUp() {
        vh = VenueViewHolder(
            VenueItemView(InstrumentationRegistry.getInstrumentation().targetContext),
            mock(VenueViewHolder.ClickListener::class.java)
        )
    }

    @Test
    fun testBind() {
        val venue = Venue("1", "A", Location("S", "C", "C"))
        vh.bind(venue)
        assertThat(vh.itemView, Matchers.instanceOf(VenueItemView::class.java))
        val venueItemView = vh.itemView as VenueItemView
        assertEquals(venueItemView.titleTextView.text, "A")
        assertEquals(venueItemView.locationTextView.text, venue.location.toString())
    }
}