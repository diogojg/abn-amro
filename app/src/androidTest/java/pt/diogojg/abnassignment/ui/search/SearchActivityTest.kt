package pt.diogojg.abnassignment.ui.search

import androidx.test.annotation.UiThreadTest
import androidx.test.rule.ActivityTestRule
import kotlinx.android.synthetic.main.activity_search.*
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.search.SearchViewState

class SearchActivityTest {
    @get:Rule
    val rule: ActivityTestRule<SearchActivity> = ActivityTestRule(SearchActivity::class.java)

    @UiThreadTest
    @Test
    fun testRenderSearchNotStartedYet() {
        rule.activity.render(SearchViewState.SearchNotStartedYet)
        val recyclerView = rule.activity.recyclerView
        val searchBarView = rule.activity.searchBarView
        assertEquals(recyclerView.adapter!!.itemCount, 0)
        assertFalse(searchBarView.isLoading)
    }

    @UiThreadTest
    @Test
    fun testRenderLoadingFirstTime() {
        rule.activity.render(SearchViewState.LoadingFirstTime)
        val recyclerView = rule.activity.recyclerView
        val searchBarView = rule.activity.searchBarView
        assertEquals(recyclerView.adapter!!.itemCount, 0)
        assertTrue(searchBarView.isLoading)
    }

    @UiThreadTest
    @Test
    fun testRenderError() {
        rule.activity.render(SearchViewState.Error(Exception()))
        val recyclerView = rule.activity.recyclerView
        val searchBarView = rule.activity.searchBarView
        assertEquals(recyclerView.adapter!!.itemCount, 0)
        assertFalse(searchBarView.isLoading)
    }

    @UiThreadTest
    @Test
    fun testRenderResult() {
        val venues = listOf(
            Venue("1", "A", Location("S", "C", "C")),
            Venue("2", "A", Location("S", "C", "C"))
        )
        val state = SearchViewState.Result(venues, true)
        rule.activity.render(state)
        val recyclerView = rule.activity.recyclerView
        val searchBarView = rule.activity.searchBarView
        assertEquals(recyclerView.adapter!!.itemCount, 2)
        assertTrue(searchBarView.isLoading)
    }
}