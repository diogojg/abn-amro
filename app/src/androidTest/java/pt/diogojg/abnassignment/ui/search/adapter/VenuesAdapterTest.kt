package pt.diogojg.abnassignment.ui.search.adapter

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue

class VenuesAdapterTest {
    private lateinit var adapter: VenuesAdapter

    @Before
    fun setUp() {
        adapter = VenuesAdapter(mock(VenuesAdapter.ClickListener::class.java))
    }

    @Test
    internal fun testItemCount() {
        val item = Venue("1", "Name", Location("Street", "City", "Country"))
        adapter.setData(listOf(item, item))
        assertEquals(adapter.itemCount, 2)
    }
}