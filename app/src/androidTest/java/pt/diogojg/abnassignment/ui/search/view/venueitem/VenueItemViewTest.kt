package pt.diogojg.abnassignment.ui.search.view.venueitem

import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.android.synthetic.main.view_venue_item.view.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue

class VenueItemViewTest{
    private lateinit var view: VenueItemView

    @Before
    fun setUp() {
        view = VenueItemView(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @Test
    fun testBind() {
        val venue = Venue("1", "A", Location("S", "C", "C"))
        view.bind(venue)
        assertEquals(view.titleTextView.text, "A")
        assertEquals(view.locationTextView.text, venue.location.toString())
    }
}