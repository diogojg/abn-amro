package pt.diogojg.abnassignment.ui.search.view.searchbar

import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.android.synthetic.main.view_search_bar.view.*
import org.junit.Before
import org.junit.Test

class SearchBarViewTest {
    lateinit var view: SearchBarView

    @Before
    fun setUp() {
        view = SearchBarView(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @Test
    fun testIsLoading_true_shouldShowLoading() {
        view.isLoading = true
        assertThat(view.searchImageView, withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE))
        assertThat(view.progressBar, withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))
    }

    @Test
    fun testIsLoading_false_shouldShowSearchIcon() {
        view.isLoading = false
        assertThat(view.searchImageView, withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))
        assertThat(view.progressBar, withEffectiveVisibility(ViewMatchers.Visibility.GONE))
    }
}