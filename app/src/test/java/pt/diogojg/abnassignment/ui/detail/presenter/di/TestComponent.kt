package pt.diogojg.abnassignment.ui.detail.presenter.di

import dagger.Component
import pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.di.VenuesRepositoryMockModule
import pt.diogojg.abnassignment.ui.detail.di.VenueDetailModule
import pt.diogojg.abnassignment.ui.detail.presenter.VenueDetailPresenterTest
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        VenueDetailModule::class,
        VenuesRepositoryMockModule::class,
        VenueDetailViewRobotModule::class
    ]
)
interface TestComponent {
    fun inject(test: VenueDetailPresenterTest)
}