package pt.diogojg.abnassignment.ui.search.presenter

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import org.junit.Assert
import pt.diogojg.abnassignment.ui.search.SearchPresenter
import pt.diogojg.abnassignment.ui.search.SearchView
import pt.diogojg.domain.search.SearchViewState
import java.util.concurrent.TimeUnit

class SearchViewRobot(presenter: SearchPresenter) {
    private val searchIntentSubject = PublishSubject.create<String>()
    private val renderEvents = ArrayList<SearchViewState>()
    private val renderEventSubject = ReplaySubject.create<SearchViewState>()

    private val view = object : SearchView {
        override fun searchIntent(): Observable<String> = searchIntentSubject

        override fun render(state: SearchViewState) {
            renderEvents.add(state)
            renderEventSubject.onNext(state)
        }
    }

    fun fireSearchIntent(place: String) {
        searchIntentSubject.onNext(place)
    }

    init {
        presenter.attachView(view)
    }

    fun assertViewStateRendered(vararg expectedViewStates: SearchViewState) {
        val eventsCount = expectedViewStates.size
        renderEventSubject.take(eventsCount.toLong())
            .timeout(5L, TimeUnit.SECONDS)
            .blockingSubscribe()

        if (renderEventSubject.values.size > eventsCount) {
            Assert.fail(
                "Expected to wait for "
                        + eventsCount
                        + ", but there were "
                        + renderEventSubject.values.size
                        + " Events in total, which is more than expected: "
                        + arrayToString(renderEventSubject.values)
            )
        }

        Assert.assertEquals(
            expectedViewStates.toList(),
            renderEvents.toList()
        )
    }

    private fun arrayToString(array: Array<Any>): String {
        val buffer = StringBuffer()
        for (o in array) {
            buffer.append(o.toString())
            buffer.append("\n")
        }

        return buffer.toString()
    }
}