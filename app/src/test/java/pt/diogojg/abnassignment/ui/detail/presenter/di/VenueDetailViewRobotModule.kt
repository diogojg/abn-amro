package pt.diogojg.abnassignment.ui.detail.presenter.di

import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.ui.detail.VenueDetailPresenter
import pt.diogojg.abnassignment.ui.detail.presenter.VenueDetailViewRobot

@Module
class VenueDetailViewRobotModule {

    @Provides
    fun provideVenueDetailViewRobot(presenter: VenueDetailPresenter): VenueDetailViewRobot =
        VenueDetailViewRobot(presenter)
}