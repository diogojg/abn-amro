package pt.diogojg.abnassignment.ui.search.presenter.di

import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.ui.search.SearchPresenter
import pt.diogojg.abnassignment.ui.search.presenter.SearchViewRobot

@Module
class SearchViewRobotModule {

    @Provides
    fun provideSearchViewRobotModule(presenter: SearchPresenter): SearchViewRobot =
        SearchViewRobot(presenter)
}