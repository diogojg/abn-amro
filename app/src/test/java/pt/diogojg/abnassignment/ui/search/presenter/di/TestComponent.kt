package pt.diogojg.abnassignment.ui.search.presenter.di

import dagger.Component
import pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.di.VenuesRepositoryMockModule
import pt.diogojg.abnassignment.ui.search.di.SearchModule
import pt.diogojg.abnassignment.ui.search.presenter.SearchPresenterTest
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        SearchModule::class,
        VenuesRepositoryMockModule::class,
        SearchViewRobotModule::class
    ]
)
interface TestComponent {
    fun inject(test: SearchPresenterTest)
}