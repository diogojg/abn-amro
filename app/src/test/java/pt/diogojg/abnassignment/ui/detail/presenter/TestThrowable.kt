package pt.diogojg.abnassignment.ui.detail.presenter

class TestThrowable : Exception("test") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}