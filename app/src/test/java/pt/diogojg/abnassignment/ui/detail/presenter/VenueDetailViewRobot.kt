package pt.diogojg.abnassignment.ui.detail.presenter

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import org.junit.Assert
import pt.diogojg.abnassignment.ui.detail.VenueDetailPresenter
import pt.diogojg.abnassignment.ui.detail.VenueDetailView
import pt.diogojg.domain.venuedetail.VenueDetailViewState
import java.util.concurrent.TimeUnit

class VenueDetailViewRobot(presenter: VenueDetailPresenter) {
    private val loadIntentSubject = PublishSubject.create<Unit>()
    private val renderEvents = ArrayList<VenueDetailViewState>()
    private val renderEventSubject = ReplaySubject.create<VenueDetailViewState>()

    private val view = object : VenueDetailView {
        override fun loadDetailsIntent(): Observable<Unit> = loadIntentSubject

        override fun render(state: VenueDetailViewState) {
            renderEvents.add(state)
            renderEventSubject.onNext(state)
        }
    }

    fun fireLoadIntent() {
        loadIntentSubject.onNext(Unit)
    }

    init {
        presenter.attachView(view)
    }

    fun assertViewStateRendered(vararg expectedViewStates: VenueDetailViewState) {
        val eventsCount = expectedViewStates.size
        renderEventSubject.take(eventsCount.toLong())
            .timeout(5L, TimeUnit.SECONDS)
            .blockingSubscribe()

        if (renderEventSubject.values.size > eventsCount) {
            Assert.fail(
                "Expected to wait for "
                        + eventsCount
                        + ", but there were "
                        + renderEventSubject.values.size
                        + " Events in total, which is more than expected: "
                        + arrayToString(renderEventSubject.values)
            )
        }

        Assert.assertEquals(
            expectedViewStates.toList(),
            renderEvents.toList()
        )
    }

    private fun arrayToString(array: Array<Any>): String {
        val buffer = StringBuffer()
        for (o in array) {
            buffer.append(o.toString())
            buffer.append("\n")
        }

        return buffer.toString()
    }
}