package pt.diogojg.abnassignment.ui.search.presenter

import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mockito
import pt.diogojg.abnassignment.ui.search.presenter.di.DaggerTestComponent
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.repository.VenuesRepository
import pt.diogojg.domain.search.SearchViewState
import javax.inject.Inject


class SearchPresenterTest {
    @Inject
    lateinit var viewRobot: SearchViewRobot

    @Inject
    lateinit var repository: VenuesRepository

    companion object {
        @BeforeClass
        @JvmStatic
        fun init() {
            // Tell RxAndroid to not use android main ui thread scheduler
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            RxAndroidPlugins.reset()
        }
    }

    @Before
    fun setUp() {
        val component = DaggerTestComponent.builder().build()
        component.inject(this)
    }

    @Test
    fun testSearchVenues_firstTime() {
        val venues = listOf(
            Venue("Id1", "B", Location(null, null, "NL")),
            Venue("Id2", "B", Location(null, null, "NL"))
        )
        Mockito.`when`(repository.getVenues("Amsterdam"))
            .thenReturn(Observable.just(venues))
        viewRobot.fireSearchIntent("Amsterdam")
        viewRobot.assertViewStateRendered(
            SearchViewState.SearchNotStartedYet,
            SearchViewState.LoadingFirstTime,
            SearchViewState.Result(venues, false)
        )
    }

    @Test
    fun testSearchVenues_twoSearches() {
        val venues = listOf(
            Venue("Id1", "B", Location(null, null, "NL")),
            Venue("Id2", "B", Location(null, null, "NL"))
        )
        Mockito.`when`(repository.getVenues(any(String::class.java)))
            .thenReturn(Observable.just(venues))
        viewRobot.fireSearchIntent("Amst")
        viewRobot.fireSearchIntent("Amsterdam")

        viewRobot.assertViewStateRendered(
            SearchViewState.SearchNotStartedYet,
            SearchViewState.LoadingFirstTime,
            SearchViewState.Result(venues, false),
            SearchViewState.Result(venues, true),
            SearchViewState.Result(venues, false)
        )
    }

    @Test
    fun testSearchVenues_error() {
        Mockito.`when`(repository.getVenues("Amsterdam"))
            .thenReturn(Observable.error(TestThrowable()))
        viewRobot.fireSearchIntent("Amsterdam")
        viewRobot.assertViewStateRendered(
            SearchViewState.SearchNotStartedYet,
            SearchViewState.LoadingFirstTime,
            SearchViewState.Error(TestThrowable())
        )
    }

    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)
}