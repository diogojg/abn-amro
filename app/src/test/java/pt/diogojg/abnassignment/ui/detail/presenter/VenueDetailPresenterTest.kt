package pt.diogojg.abnassignment.ui.detail.presenter

import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mockito
import pt.diogojg.abnassignment.ui.detail.di.VenueDetailModule
import pt.diogojg.abnassignment.ui.detail.presenter.di.DaggerTestComponent
import pt.diogojg.domain.model.ContactInformation
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.model.VenueDetail
import pt.diogojg.domain.repository.VenuesRepository
import pt.diogojg.domain.venuedetail.VenueDetailViewState
import javax.inject.Inject

class VenueDetailPresenterTest {
    @Inject
    lateinit var viewRobot: VenueDetailViewRobot

    @Inject
    lateinit var repository: VenuesRepository

    private val venue = Venue("Id1", "B", Location(null, null, "NL"))

    companion object {
        @BeforeClass
        @JvmStatic
        fun init() {
            // Tell RxAndroid to not use android main ui thread scheduler
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            RxAndroidPlugins.reset()
        }
    }

    @Before
    fun setUp() {
        val component = DaggerTestComponent.builder()
            .venueDetailModule(VenueDetailModule(venue))
            .build()
        component.inject(this)
    }

    @Test
    fun testGetDetail_onSuccess() {

        val venueDetail = VenueDetail(
            venue.id,
            venue.name,
            9.0,
            null,
            null,
            ContactInformation(null, null, null, null),
            venue.location
        )
        Mockito.`when`(repository.getDetail(venue))
            .thenReturn(Observable.just(venueDetail))
        viewRobot.fireLoadIntent()
        viewRobot.assertViewStateRendered(
            VenueDetailViewState.Loading,
            VenueDetailViewState.Result(venueDetail)
        )
    }

    @Test
    fun testGetDetail_onError() {
        Mockito.`when`(repository.getDetail(venue))
            .thenReturn(Observable.error(TestThrowable()))
        viewRobot.fireLoadIntent()
        viewRobot.assertViewStateRendered(
            VenueDetailViewState.Loading,
            VenueDetailViewState.Error(TestThrowable())
        )
    }
}