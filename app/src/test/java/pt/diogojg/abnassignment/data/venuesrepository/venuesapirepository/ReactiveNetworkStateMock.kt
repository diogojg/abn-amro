package pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import pt.diogojg.abnassignment.data.retrofit.internetstatus.ReactiveNetworkState

class ReactiveNetworkStateMock : ReactiveNetworkState {

    private val subject = PublishSubject.create<Unit>()

    override fun hasConnectionBecomeAvailable(): Observable<Unit> = subject

    fun triggerConnectionAvailable() {
        subject.onNext(Unit)
    }
}