package pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.di

import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock
import pt.diogojg.abnassignment.data.retrofit.facade.FoursquareRepositoryFacade
import pt.diogojg.abnassignment.data.venuesrepository.VenuesApiRepository
import pt.diogojg.abnassignment.data.venuesrepository.di.MappersModule
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueDetailMapper
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueMapper
import pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.ReactiveNetworkStateMock
import javax.inject.Singleton

@Module(includes = [MappersModule::class])
class TestModule {

    @Singleton
    @Provides
    fun provideVenuesRepository(
        facade: FoursquareRepositoryFacade,
        networkStateCallback: ReactiveNetworkStateMock,
        venueMapper: VenueMapper,
        venueDetailMapper: VenueDetailMapper
    ): VenuesApiRepository = VenuesApiRepository(facade, networkStateCallback, venueMapper, venueDetailMapper)

    @Singleton
    @Provides
    fun provideFoursquareRepositoryFacade(): FoursquareRepositoryFacade = mock(
        FoursquareRepositoryFacade::class.java
    )

    @Singleton
    @Provides
    fun provideNetworkStateCallback(): ReactiveNetworkStateMock = ReactiveNetworkStateMock()
}