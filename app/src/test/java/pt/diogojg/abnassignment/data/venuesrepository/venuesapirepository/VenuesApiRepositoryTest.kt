package pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository

import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mockito
import pt.diogojg.abnassignment.data.retrofit.contract.ResponseWrapper
import pt.diogojg.abnassignment.data.retrofit.contract.detail.*
import pt.diogojg.abnassignment.data.retrofit.contract.search.LocationItem
import pt.diogojg.abnassignment.data.retrofit.contract.search.Response
import pt.diogojg.abnassignment.data.retrofit.contract.search.VenuesSearchResponse
import pt.diogojg.abnassignment.data.retrofit.facade.FoursquareRepositoryFacade
import pt.diogojg.abnassignment.data.venuesrepository.VenuesApiRepository
import pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.di.DaggerTestComponent
import pt.diogojg.domain.model.Location
import pt.diogojg.domain.model.Venue
import javax.inject.Inject

class VenuesApiRepositoryTest {

    @Inject
    lateinit var venuesApiRepository: VenuesApiRepository

    @Inject
    lateinit var foursquareRepositoryFacade: FoursquareRepositoryFacade

    @Inject
    lateinit var reactiveNetworkState: ReactiveNetworkStateMock

    companion object {
        @BeforeClass
        @JvmStatic
        fun init() {
            // Tell RxAndroid to not use android main ui thread scheduler
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            RxAndroidPlugins.reset()
        }
    }

    @Before
    fun setUp() {
        val component = DaggerTestComponent.create()
        component.inject(this)
    }

    @Test
    fun testGetVenues_dataFromNetwork_shouldFetchDataOnce() {
        Mockito.`when`(foursquareRepositoryFacade.venuesSearch("Lisbon"))
            .thenReturn(Single.just(ResponseWrapper(mockResponseEmptyVenueList(), ResponseWrapper.Origin.NETWORK)))
        val testObserver = venuesApiRepository.getVenues("Lisbon").test()
        reactiveNetworkState.triggerConnectionAvailable()

        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.dispose()
    }

    @Test
    fun testGetVenues_dataFromCache_shouldFetchDataTwice() {
        Mockito.`when`(foursquareRepositoryFacade.venuesSearch("Lisbon"))
            .thenReturn(Single.just(ResponseWrapper(mockResponseEmptyVenueList(), ResponseWrapper.Origin.CACHE)))
            .thenReturn(Single.just(ResponseWrapper(mockResponseEmptyVenueList(), ResponseWrapper.Origin.NETWORK)))
        val testObserver = venuesApiRepository.getVenues("Lisbon").test()

        // multiple triggers to check if it stops fetching after data comes from the network
        reactiveNetworkState.triggerConnectionAvailable()
        reactiveNetworkState.triggerConnectionAvailable()

        testObserver.assertValueCount(2)
        testObserver.assertComplete()
        testObserver.dispose()
    }

    private fun mockResponseEmptyVenueList() = VenuesSearchResponse(Response(listOf()))

    @Test
    fun testGetVenueDetail_dataFromNetwork_shouldFetchDataOnce() {
        val venue = Venue("id", "name", Location(null, null, "NL"))
        Mockito.`when`(foursquareRepositoryFacade.getVenueDetail(venue.id))
            .thenReturn(Single.just(ResponseWrapper(mockResponseVenueDetail(), ResponseWrapper.Origin.NETWORK)))

        val testObserver = venuesApiRepository.getDetail(venue).test()
        reactiveNetworkState.triggerConnectionAvailable()

        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.dispose()
    }

    @Test
    fun testGetVenueDetail_dataFromCache_shouldFetchDataTwice() {
        val venue = Venue("id", "name", Location(null, null, "NL"))
        Mockito.`when`(foursquareRepositoryFacade.getVenueDetail(venue.id))
            .thenReturn(Single.just(ResponseWrapper(mockResponseVenueDetail(), ResponseWrapper.Origin.CACHE)))
            .thenReturn(Single.just(ResponseWrapper(mockResponseVenueDetail(), ResponseWrapper.Origin.NETWORK)))

        val testObserver = venuesApiRepository.getDetail(venue).test()
        reactiveNetworkState.triggerConnectionAvailable()

        testObserver.assertValueCount(2)
        testObserver.assertComplete()
        testObserver.dispose()
    }

    private fun mockResponseVenueDetail() = VenueDetailContract(
        DetailResponse(
            VenueItem(
                BestPhoto(10, "p", "s", 10),
                Contact(null, null, null, null, null, null, null),
                "description",
                "Id",
                LocationItem(null, null, "NL"),
                "Name",
                9.5
            )
        )
    )
}