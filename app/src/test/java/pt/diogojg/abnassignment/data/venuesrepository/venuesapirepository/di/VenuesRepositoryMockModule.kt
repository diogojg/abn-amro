package pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.di

import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import pt.diogojg.domain.repository.VenuesRepository
import javax.inject.Singleton

@Module
class VenuesRepositoryMockModule {

    @Singleton
    @Provides
    fun provideVenuesRepository(): VenuesRepository = Mockito.mock(VenuesRepository::class.java)
}