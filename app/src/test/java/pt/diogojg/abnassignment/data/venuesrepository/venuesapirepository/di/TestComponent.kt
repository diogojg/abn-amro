package pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.di

import dagger.Component
import pt.diogojg.abnassignment.data.venuesrepository.venuesapirepository.VenuesApiRepositoryTest
import javax.inject.Singleton

@Singleton
@Component(modules = [TestModule::class])
interface TestComponent {
    fun inject(test: VenuesApiRepositoryTest)
}