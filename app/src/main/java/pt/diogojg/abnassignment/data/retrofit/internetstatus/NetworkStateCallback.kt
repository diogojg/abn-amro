package pt.diogojg.abnassignment.data.retrofit.internetstatus

import android.net.ConnectivityManager
import android.net.Network
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class NetworkStateCallback : ReactiveNetworkState, ConnectivityManager.NetworkCallback() {
    private val hasConnectionAvailableSubject = PublishSubject.create<Unit>()
    private var hasConnection: Boolean? = null

    override fun onAvailable(network: Network?) {
        super.onAvailable(network)
        val hasConnection = this.hasConnection
        if (hasConnection != null && !hasConnection)
            hasConnectionAvailableSubject.onNext(Unit)
        this.hasConnection = true
    }

    override fun onLost(network: Network?) {
        super.onLost(network)
        this.hasConnection = false
    }

    override fun hasConnectionBecomeAvailable(): Observable<Unit> = hasConnectionAvailableSubject.hide()
}