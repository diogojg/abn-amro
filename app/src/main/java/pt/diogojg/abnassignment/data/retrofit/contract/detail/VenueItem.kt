package pt.diogojg.abnassignment.data.retrofit.contract.detail


import com.google.gson.annotations.SerializedName
import pt.diogojg.abnassignment.data.retrofit.contract.search.LocationItem

data class VenueItem(
    @SerializedName("bestPhoto")
    val bestPhoto: BestPhoto?,
    @SerializedName("contact")
    val contact: Contact,
    @SerializedName("description")
    val description: String?,
    @SerializedName("id")
    val id: String,
    val location: LocationItem,
    @SerializedName("name")
    val name: String,
    @SerializedName("rating")
    val rating: Double
)