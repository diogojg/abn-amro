package pt.diogojg.abnassignment.data.retrofit.internetstatus

import io.reactivex.Observable

interface ReactiveNetworkState {

    fun hasConnectionBecomeAvailable(): Observable<Unit>
}