package pt.diogojg.abnassignment.data.retrofit.facade

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import pt.diogojg.abnassignment.data.retrofit.FoursquareService
import pt.diogojg.abnassignment.data.retrofit.contract.ResponseWrapper
import pt.diogojg.abnassignment.data.retrofit.contract.detail.VenueDetailContract
import pt.diogojg.abnassignment.data.retrofit.contract.search.VenuesSearchResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FoursquareServiceFacade(
    private val service: FoursquareService,
    private val clientId: String,
    private val clientSecret: String
) : FoursquareRepositoryFacade {

    override fun venuesSearch(near: String): Single<ResponseWrapper<VenuesSearchResponse>> =
        service.venuesSearch(clientId, clientSecret, 5, 1000, near)
            .convertToSingle()
            .subscribeOn(Schedulers.io())

    override fun getVenueDetail(venueId: String): Single<ResponseWrapper<VenueDetailContract>> =
        service.venueDetail(venueId, clientId, clientSecret)
            .convertToSingle()
            .subscribeOn(Schedulers.io())

    private fun <T> Call<T>.convertToSingle(): Single<ResponseWrapper<T>> = Single.create {
        this.clone().enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                it.onError(t)
            }

            override fun onResponse(
                call: Call<T>,
                response: Response<T>
            ) {
                val responseBody = response.body()
                if (responseBody == null) {
                    it.onError(Exception("DetailResponse body cannot be null."))
                } else {
                    val origin = if (response.raw().networkResponse() != null) ResponseWrapper.Origin.NETWORK
                    else ResponseWrapper.Origin.CACHE
                    it.onSuccess(ResponseWrapper(responseBody, origin))
                }
            }
        })
    }
}