package pt.diogojg.abnassignment.data.venuesrepository.mapper

import pt.diogojg.abnassignment.data.retrofit.contract.detail.Contact
import pt.diogojg.domain.model.ContactInformation

class ContactMapper {
    fun map(contact: Contact): ContactInformation = ContactInformation(
        contact.facebookUsername,
        contact.phone,
        contact.instagram,
        contact.twitter
    )
}