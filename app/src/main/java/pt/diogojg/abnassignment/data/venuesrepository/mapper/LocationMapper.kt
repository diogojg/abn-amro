package pt.diogojg.abnassignment.data.venuesrepository.mapper

import pt.diogojg.abnassignment.data.retrofit.contract.search.LocationItem
import pt.diogojg.domain.model.Location

class LocationMapper {
    fun map(location: LocationItem): Location = Location(
        location.address,
        location.city,
        location.country
    )
}