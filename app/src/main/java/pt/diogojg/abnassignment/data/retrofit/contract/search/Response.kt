package pt.diogojg.abnassignment.data.retrofit.contract.search


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("venues")
    val venues: List<VenueItem>
)