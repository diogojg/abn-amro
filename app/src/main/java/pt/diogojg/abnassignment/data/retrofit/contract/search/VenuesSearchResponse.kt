package pt.diogojg.abnassignment.data.retrofit.contract.search


import com.google.gson.annotations.SerializedName

data class VenuesSearchResponse(
    @SerializedName("response")
    val response: Response
)