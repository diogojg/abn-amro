package pt.diogojg.abnassignment.data.venuesrepository

import io.reactivex.Observable
import io.reactivex.Single
import pt.diogojg.abnassignment.data.retrofit.contract.ResponseWrapper
import pt.diogojg.abnassignment.data.retrofit.facade.FoursquareRepositoryFacade
import pt.diogojg.abnassignment.data.retrofit.internetstatus.ReactiveNetworkState
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueDetailMapper
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueMapper
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.model.VenueDetail
import pt.diogojg.domain.repository.VenuesRepository

class VenuesApiRepository(
    private val facade: FoursquareRepositoryFacade,
    private val networkStateCallback: ReactiveNetworkState,
    private val venueMapper: VenueMapper,
    private val venueDetailMapper: VenueDetailMapper
) : VenuesRepository {
    override fun getVenues(query: String): Observable<List<Venue>> =
        repeatRequestUntilOriginIsNetwork { facade.venuesSearch(query) }
            .map { it.data.response.venues.map { venueMapper.map(it) } }

    override fun getDetail(venue: Venue): Observable<VenueDetail> =
        repeatRequestUntilOriginIsNetwork { facade.getVenueDetail(venue.id) }
            .map { venueDetailMapper.map(it.data.response.venue) }

    private fun <T> repeatRequestUntilOriginIsNetwork(request: () -> Single<ResponseWrapper<T>>)
            : Observable<ResponseWrapper<T>> =
        request.invoke().toObservable().flatMap {
            when (it.origin) {
                ResponseWrapper.Origin.CACHE -> Observable.just(it)
                    .concatWith(performRequestWhenNetworkBecomesAvailable { request.invoke() })
                ResponseWrapper.Origin.NETWORK -> Observable.just(it)
            }
        }

    private fun <T> performRequestWhenNetworkBecomesAvailable(request: () -> Single<ResponseWrapper<T>>)
            : Observable<ResponseWrapper<T>> =
        networkStateCallback.hasConnectionBecomeAvailable()
            .concatMap { request.invoke().toObservable() }
            .takeUntil { it.origin == ResponseWrapper.Origin.NETWORK }
}