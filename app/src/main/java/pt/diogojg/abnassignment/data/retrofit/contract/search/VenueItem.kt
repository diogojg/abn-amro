package pt.diogojg.abnassignment.data.retrofit.contract.search


import com.google.gson.annotations.SerializedName

data class VenueItem(
    @SerializedName("id")
    val id: String,
    @SerializedName("location")
    val location: LocationItem,
    @SerializedName("name")
    val name: String
)