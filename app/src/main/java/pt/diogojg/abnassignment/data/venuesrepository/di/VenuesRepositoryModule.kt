package pt.diogojg.abnassignment.data.venuesrepository.di

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import pt.diogojg.abnassignment.data.retrofit.FoursquareService
import pt.diogojg.abnassignment.data.retrofit.OfflineCacheInterceptor
import pt.diogojg.abnassignment.data.retrofit.facade.FoursquareServiceFacade
import pt.diogojg.abnassignment.data.retrofit.internetstatus.InternetStatusProvider
import pt.diogojg.abnassignment.data.retrofit.internetstatus.NetworkStateCallback
import pt.diogojg.abnassignment.data.retrofit.internetstatus.SimpleInternetStatusProvider
import pt.diogojg.abnassignment.data.venuesrepository.VenuesApiRepository
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueDetailMapper
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueMapper
import pt.diogojg.domain.repository.VenuesRepository
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [MappersModule::class])
class VenuesRepositoryModule(
    private val clientId: String,
    private val clientSecret: String,
    private val baseUrl: String
) {

    @Singleton
    @Provides
    fun provideVenuesRepository(
        facade: FoursquareServiceFacade,
        networkStateCallback: NetworkStateCallback,
        venueMapper: VenueMapper,
        venueDetailMapper: VenueDetailMapper
    ): VenuesRepository = VenuesApiRepository(facade, networkStateCallback, venueMapper, venueDetailMapper)

    @Singleton
    @Provides
    fun provideFoursquareServiceFacade(service: FoursquareService): FoursquareServiceFacade =
        FoursquareServiceFacade(service, clientId, clientSecret)

    @Singleton
    @Provides
    fun provideFoursquareService(retrofit: Retrofit): FoursquareService =
        retrofit.create(FoursquareService::class.java)

    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun provideOkHttpClient(
        cache: Cache,
        interceptor: Interceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .cache(cache)
        .addInterceptor(interceptor)
        .build()

    @Singleton
    @Provides
    fun provideCache(context: Context): Cache =
        Cache(context.cacheDir, (10 * 1024 * 1024).toLong()) // 10MB

    @Singleton
    @Provides
    fun provideInterceptor(internetStatusProvider: InternetStatusProvider): Interceptor =
        OfflineCacheInterceptor(internetStatusProvider)

    @Singleton
    @Provides
    fun provideInternetStatusProvider(
        context: Context
    ): InternetStatusProvider = SimpleInternetStatusProvider(context)

    @Singleton
    @Provides
    fun provideNetworkStateCallback(): NetworkStateCallback = NetworkStateCallback()
}