package pt.diogojg.abnassignment.data.venuesrepository.mapper

import pt.diogojg.abnassignment.data.retrofit.contract.search.VenueItem
import pt.diogojg.domain.model.Venue

class VenueMapper(private val locationMapper: LocationMapper) {
    fun map(venueItem: VenueItem): Venue = Venue(
        venueItem.id,
        venueItem.name,
        locationMapper.map(venueItem.location)
    )
}