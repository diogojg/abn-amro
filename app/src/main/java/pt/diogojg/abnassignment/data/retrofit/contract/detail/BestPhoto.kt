package pt.diogojg.abnassignment.data.retrofit.contract.detail


import com.google.gson.annotations.SerializedName

data class BestPhoto(
    @SerializedName("height")
    val height: Int,
    @SerializedName("prefix")
    val prefix: String,
    @SerializedName("suffix")
    val suffix: String,
    @SerializedName("width")
    val width: Int
)