package pt.diogojg.abnassignment.data.retrofit.contract.detail


import com.google.gson.annotations.SerializedName

data class VenueDetailContract(
    @SerializedName("response")
    val response: DetailResponse
)