package pt.diogojg.abnassignment.data.venuesrepository.mapper

import pt.diogojg.abnassignment.data.retrofit.contract.detail.BestPhoto
import pt.diogojg.abnassignment.data.retrofit.contract.detail.VenueItem
import pt.diogojg.domain.model.VenueDetail

class VenueDetailMapper(
    private val locationMapper: LocationMapper,
    private val contactMapper: ContactMapper
) {
    fun map(venueItem: VenueItem): VenueDetail = VenueDetail(
        venueItem.id,
        venueItem.name,
        venueItem.rating,
        venueItem.bestPhoto?.getPhotoUrl(),
        venueItem.description,
        contactMapper.map(venueItem.contact),
        locationMapper.map(venueItem.location)
    )

    private fun BestPhoto.getPhotoUrl() = prefix + width + "x" + height + suffix
}