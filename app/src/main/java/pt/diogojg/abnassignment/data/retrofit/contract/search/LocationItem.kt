package pt.diogojg.abnassignment.data.retrofit.contract.search


import com.google.gson.annotations.SerializedName

data class LocationItem(
    @SerializedName("address")
    val address: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("country")
    val country: String
)