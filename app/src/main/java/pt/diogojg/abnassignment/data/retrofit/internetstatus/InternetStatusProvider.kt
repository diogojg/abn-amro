package pt.diogojg.abnassignment.data.retrofit.internetstatus

interface InternetStatusProvider {
    fun hasInternet(): Boolean
}