package pt.diogojg.abnassignment.data.venuesrepository.di

import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.data.venuesrepository.mapper.ContactMapper
import pt.diogojg.abnassignment.data.venuesrepository.mapper.LocationMapper
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueDetailMapper
import pt.diogojg.abnassignment.data.venuesrepository.mapper.VenueMapper
import javax.inject.Singleton

@Module
class MappersModule {
    @Singleton
    @Provides
    fun provideVenueMapper(locationMapper: LocationMapper): VenueMapper = VenueMapper(locationMapper)

    @Singleton
    @Provides
    fun provideLocationMapper(): LocationMapper = LocationMapper()

    @Singleton
    @Provides
    fun provideVenueDetailMapper(locationMapper: LocationMapper, contactMapper: ContactMapper): VenueDetailMapper =
        VenueDetailMapper(locationMapper, contactMapper)

    @Singleton
    @Provides
    fun provideContactMapper(): ContactMapper = ContactMapper()
}