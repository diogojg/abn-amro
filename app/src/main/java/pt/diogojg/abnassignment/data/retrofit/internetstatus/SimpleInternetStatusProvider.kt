package pt.diogojg.abnassignment.data.retrofit.internetstatus

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class SimpleInternetStatusProvider(private val context: Context) : InternetStatusProvider {
    //source: https://medium.com/mindorks/caching-with-retrofit-store-responses-offline-71439ed32fda
    override fun hasInternet(): Boolean {
        var isConnected = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }
}