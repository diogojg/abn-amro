package pt.diogojg.abnassignment.data.retrofit

import pt.diogojg.abnassignment.data.retrofit.contract.detail.VenueDetailContract
import pt.diogojg.abnassignment.data.retrofit.contract.search.VenuesSearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoursquareService {
    companion object {
        private const val VERSION = "20190720"
    }

    @GET("/v2/venues/search")
    fun venuesSearch(
        @Query("client_id") clientId: String, //Could be injected in a OkHttp interceptor
        @Query("client_secret") clientSecret: String, //Could be injected in a OkHttp interceptor
        @Query("limit") limit: Int,
        @Query("radius") radius: Int,
        @Query("near") near: String,
        @Query("v") version: String = VERSION //Could be injected in a OkHttp interceptor
    ): Call<VenuesSearchResponse>

    @GET("/v2/venues/{venueId}")
    fun venueDetail(
        @Path("venueId") venueId: String,
        @Query("client_id") clientId: String, //Could be injected in a OkHttp interceptor
        @Query("client_secret") clientSecret: String, //Could be injected in a OkHttp interceptor
        @Query("v") version: String = VERSION //Could be injected in a OkHttp interceptor
    ): Call<VenueDetailContract>
}