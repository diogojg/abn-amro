package pt.diogojg.abnassignment.data.retrofit.contract

data class ResponseWrapper<T>(val data: T, val origin: Origin) {

    enum class Origin {
        NETWORK,
        CACHE
    }
}