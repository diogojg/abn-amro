package pt.diogojg.abnassignment.data.retrofit.contract.detail


import com.google.gson.annotations.SerializedName

data class DetailResponse(
    @SerializedName("venue")
    val venue: VenueItem
)