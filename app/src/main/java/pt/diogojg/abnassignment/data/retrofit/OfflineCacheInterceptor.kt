package pt.diogojg.abnassignment.data.retrofit

import okhttp3.Interceptor
import okhttp3.Response
import pt.diogojg.abnassignment.data.retrofit.internetstatus.InternetStatusProvider

class OfflineCacheInterceptor(private val internetStatusProvider: InternetStatusProvider) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = if (internetStatusProvider.hasInternet())
            request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
        else
            request.newBuilder().header(
                "Cache-Control",
                "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
            ).build()
        return chain.proceed(request)
    }
}