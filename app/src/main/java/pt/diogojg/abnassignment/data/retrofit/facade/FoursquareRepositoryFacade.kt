package pt.diogojg.abnassignment.data.retrofit.facade

import io.reactivex.Single
import pt.diogojg.abnassignment.data.retrofit.contract.ResponseWrapper
import pt.diogojg.abnassignment.data.retrofit.contract.detail.VenueDetailContract
import pt.diogojg.abnassignment.data.retrofit.contract.search.VenuesSearchResponse

interface FoursquareRepositoryFacade {
    fun venuesSearch(near: String): Single<ResponseWrapper<VenuesSearchResponse>>

    fun getVenueDetail(venueId: String): Single<ResponseWrapper<VenueDetailContract>>
}