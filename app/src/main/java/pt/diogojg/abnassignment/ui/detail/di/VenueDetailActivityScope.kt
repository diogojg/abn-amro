package pt.diogojg.abnassignment.ui.detail.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class VenueDetailActivityScope