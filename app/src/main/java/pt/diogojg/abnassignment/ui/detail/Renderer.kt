package pt.diogojg.abnassignment.ui.detail

import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_venue_detail.*
import pt.diogojg.domain.model.VenueDetail
import pt.diogojg.domain.venuedetail.VenueDetailViewState

class Renderer(
    private val activity: VenueDetailActivity,
    private val picasso: Picasso
) {
    fun render(state: VenueDetailViewState) = when (state) {
        is VenueDetailViewState.Loading -> renderLoadingState()
        is VenueDetailViewState.Result -> renderResultState(state.venueDetail)
        is VenueDetailViewState.Error -> renderErrorState(state.throwable)
    }

    private fun renderLoadingState() {
        //TODO: show progress bar
    }

    private fun renderResultState(venueDetail: VenueDetail) {
        if (!venueDetail.bestPhotoUrl.isNullOrBlank())
            picasso.load(venueDetail.bestPhotoUrl).fit().centerCrop().into(activity.imageView)
        activity.nameTextView.text = venueDetail.name
        activity.ratingTextView.text = venueDetail.rating.toString()
        activity.descriptionTextView.text =
            venueDetail.description ?: "No description available" //TODO: Should be a localized string
        activity.addressTextView.text = venueDetail.location.toString()
        activity.contactInformationTextView.text = venueDetail.contactInformation.toString()
    }

    private fun renderErrorState(throwable: Throwable) {
        //TODO: parse throwable and show error to the user
    }
}