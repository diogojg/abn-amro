package pt.diogojg.abnassignment.ui.search.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SearchActivityScope