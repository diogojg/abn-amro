package pt.diogojg.abnassignment.ui.detail.di

import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.ui.detail.Renderer
import pt.diogojg.abnassignment.ui.detail.VenueDetailActivity

@Module
class VenueDetailActivityModule(private val activity: VenueDetailActivity) {
    @Provides
    fun provideRenderer(picasso: Picasso): Renderer = Renderer(activity, picasso)
}