package pt.diogojg.abnassignment.ui.detail.di

import dagger.Component
import pt.diogojg.abnassignment.application.di.ApplicationComponent
import pt.diogojg.abnassignment.ui.detail.VenueDetailActivity
import pt.diogojg.abnassignment.ui.detail.VenueDetailPresenter

@VenueDetailActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [VenueDetailActivityModule::class,
        VenueDetailModule::class]
)
interface VenueDetailActivityComponent {
    fun inject(activity: VenueDetailActivity)

    fun newVenueDetailPresenter(): VenueDetailPresenter
}