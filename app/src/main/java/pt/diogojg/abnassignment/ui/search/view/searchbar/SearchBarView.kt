package pt.diogojg.abnassignment.ui.search.view.searchbar

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.view_search_bar.view.*
import pt.diogojg.abnassignment.R

class SearchBarView : ConstraintLayout {
    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_search_bar, this)
        setBackgroundResource(R.drawable.search_bar_bg)
    }

    var isLoading: Boolean = false
        set(value) {
            field = value
            if (value) {
                searchImageView.visibility = INVISIBLE
                progressBar.visibility = VISIBLE
            } else {
                searchImageView.visibility = VISIBLE
                progressBar.visibility = GONE
            }
        }
}