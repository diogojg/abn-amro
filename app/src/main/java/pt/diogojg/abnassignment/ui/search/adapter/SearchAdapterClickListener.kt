package pt.diogojg.abnassignment.ui.search.adapter

import android.content.Context
import pt.diogojg.abnassignment.ui.detail.VenueDetailActivity
import pt.diogojg.domain.model.Venue

class SearchAdapterClickListener(private val context: Context) : VenuesAdapter.ClickListener {
    override fun onCLickListener(venue: Venue) {
        context.startActivity(VenueDetailActivity.createIntent(context, venue))
    }
}