package pt.diogojg.abnassignment.ui.search

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.view_search_bar.view.*
import pt.diogojg.abnassignment.application.BaseApplication
import pt.diogojg.abnassignment.ui.search.adapter.VenuesAdapter
import pt.diogojg.abnassignment.ui.search.di.DaggerSearchActivityComponent
import pt.diogojg.abnassignment.ui.search.di.SearchActivityModule
import pt.diogojg.domain.search.SearchViewState
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchActivity : MviActivity<SearchView, SearchPresenter>(), SearchView {
    @Inject
    lateinit var adapter: VenuesAdapter

    @Inject
    lateinit var renderer: Renderer

    @Inject
    lateinit var itemDecoration: RecyclerView.ItemDecoration

    private val component by lazy {
        DaggerSearchActivityComponent.builder()
            .searchActivityModule(SearchActivityModule(this))
            .applicationComponent(BaseApplication.instance.appComponent)
            .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(pt.diogojg.abnassignment.R.layout.activity_search)
        component.inject(this)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(itemDecoration)
        (recyclerView.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }

    override fun createPresenter(): SearchPresenter = component.newSearchPresenter()

    override fun searchIntent(): Observable<String> = searchBarView.searchEditText.textChanges()
        .filter { !isRestoringViewState }
        .skip(1) // Because after screen orientation change the query shouldn'data be resubmitted again
        .debounce(500, TimeUnit.MILLISECONDS)
        .map { it.toString() }

    override fun render(state: SearchViewState) = renderer.render(state)
}
