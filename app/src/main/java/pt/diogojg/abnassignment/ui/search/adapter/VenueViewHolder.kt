package pt.diogojg.abnassignment.ui.search.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.recyclerview.widget.RecyclerView
import pt.diogojg.abnassignment.R
import pt.diogojg.abnassignment.ui.search.view.venueitem.VenueItemView
import pt.diogojg.domain.model.Venue

class VenueViewHolder(
    private val view: VenueItemView,
    private val clickListener: ClickListener
) : RecyclerView.ViewHolder(view), View.OnClickListener {
    init {
        view.setOnClickListener(this)
    }

    fun bind(venue: Venue) = view.bind(venue)

    override fun onClick(v: View?) {
        clickListener.onClickListener(adapterPosition)
    }

    companion object {
        fun create(context: Context, clickListener: ClickListener): VenueViewHolder {
            val view = VenueItemView(context).apply {
                layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            }
            view.elevation = context.resources.getDimension(R.dimen.card_elevation)
            return VenueViewHolder(view, clickListener)
        }
    }

    interface ClickListener {
        fun onClickListener(position: Int)
    }
}
