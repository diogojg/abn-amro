package pt.diogojg.abnassignment.ui.search

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable
import pt.diogojg.domain.search.SearchViewState

interface SearchView : MvpView {
    fun searchIntent(): Observable<String>

    fun render(state: SearchViewState)
}