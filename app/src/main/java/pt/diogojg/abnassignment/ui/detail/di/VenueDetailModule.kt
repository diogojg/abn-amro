package pt.diogojg.abnassignment.ui.detail.di

import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.ui.detail.VenueDetailPresenter
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.repository.VenuesRepository
import pt.diogojg.domain.venuedetail.VenueDetailInteractor

@Module
class VenueDetailModule(private val venue: Venue) {
    @Provides
    fun provideVenueDetailPresenter(interactor: VenueDetailInteractor): VenueDetailPresenter =
        VenueDetailPresenter(interactor)

    @Provides
    fun provideVenueDetailInteractor(repository: VenuesRepository): VenueDetailInteractor =
        VenueDetailInteractor(repository, venue)
}