package pt.diogojg.abnassignment.ui.search

import kotlinx.android.synthetic.main.activity_search.*
import pt.diogojg.domain.search.SearchViewState

class Renderer(private val activity: SearchActivity) {
    fun render(state: SearchViewState) {
        when (state) {
            is SearchViewState.SearchNotStartedYet -> renderSearchNotStartedYet()
            is SearchViewState.LoadingFirstTime -> renderLoadingFirstTime()
            is SearchViewState.Result -> renderResult(state)
            is SearchViewState.Error -> renderError()
        }
    }

    private fun renderSearchNotStartedYet() {
        activity.searchBarView.isLoading = false
        activity.adapter.setData(emptyList())
        //TODO: Show user friendly message
    }

    private fun renderLoadingFirstTime() {
        activity.searchBarView.isLoading = true
        activity.adapter.setData(emptyList())
    }

    private fun renderResult(state: SearchViewState.Result) {
        activity.searchBarView.isLoading = state.isLoading
        activity.adapter.setData(state.venues)
    }

    private fun renderError() {
        activity.searchBarView.isLoading = false
        activity.adapter.setData(emptyList())
        //TODO: Show the error to the user
    }
}