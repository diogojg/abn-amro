package pt.diogojg.abnassignment.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.hannesdorfmann.mosby3.mvi.MviActivity
import io.reactivex.Observable
import pt.diogojg.abnassignment.R
import pt.diogojg.abnassignment.application.BaseApplication
import pt.diogojg.abnassignment.ui.detail.di.DaggerVenueDetailActivityComponent
import pt.diogojg.abnassignment.ui.detail.di.VenueDetailActivityModule
import pt.diogojg.abnassignment.ui.detail.di.VenueDetailModule
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.venuedetail.VenueDetailViewState
import javax.inject.Inject

class VenueDetailActivity : MviActivity<VenueDetailView, VenueDetailPresenter>(), VenueDetailView {
    @Inject
    lateinit var renderer: Renderer

    private val component by lazy {
        DaggerVenueDetailActivityComponent.builder()
            .venueDetailActivityModule(VenueDetailActivityModule(this))
            .venueDetailModule(VenueDetailModule(venueFromExtras))
            .applicationComponent(BaseApplication.instance.appComponent)
            .build()
    }

    companion object {
        private const val EXTRAS_VENUE = "venue"
        fun createIntent(context: Context, venue: Venue): Intent =
            Intent(context, VenueDetailActivity::class.java).apply {
                putExtra(EXTRAS_VENUE, venue)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venue_detail)
        component.inject(this)
    }

    override fun createPresenter(): VenueDetailPresenter = component.newVenueDetailPresenter()

    override fun loadDetailsIntent(): Observable<Unit> = Observable.just(Unit)
        .filter { !isRestoringViewState }

    override fun render(state: VenueDetailViewState) = renderer.render(state)

    private val venueFromExtras: Venue
        get() = intent.getSerializableExtra(EXTRAS_VENUE) as Venue
}