package pt.diogojg.abnassignment.ui.search.adapter

import androidx.recyclerview.widget.DiffUtil
import pt.diogojg.domain.model.Venue


class ValuesDiffCallback(private val oldItems: List<Venue>, private val newItems: List<Venue>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldItems[oldItemPosition].id == newItems[newItemPosition].id

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldItems[oldItemPosition] == newItems[newItemPosition]
}