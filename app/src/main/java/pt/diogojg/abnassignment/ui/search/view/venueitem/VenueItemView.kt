package pt.diogojg.abnassignment.ui.search.view.venueitem

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.view_venue_item.view.*
import pt.diogojg.abnassignment.R
import pt.diogojg.domain.model.Venue

class VenueItemView : ConstraintLayout {
    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_venue_item, this)
        setBackgroundResource(R.drawable.venue_item_bg)
        val padding = resources.getDimensionPixelSize(R.dimen.card_padding)
        setPadding(padding, padding, padding, padding)
    }

    fun bind(venue: Venue) {
        titleTextView.text = venue.name
        locationTextView.text = venue.location.toString()
    }
}