package pt.diogojg.abnassignment.ui.detail

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable
import pt.diogojg.domain.venuedetail.VenueDetailViewState

interface VenueDetailView : MvpView {

    fun loadDetailsIntent(): Observable<Unit>

    fun render(state: VenueDetailViewState)
}