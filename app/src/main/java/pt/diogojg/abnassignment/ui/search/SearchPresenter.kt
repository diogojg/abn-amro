package pt.diogojg.abnassignment.ui.search

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import pt.diogojg.domain.search.SearchInteractor
import pt.diogojg.domain.search.SearchViewState

class SearchPresenter(private val interactor: SearchInteractor) : MviBasePresenter<SearchView, SearchViewState>() {

    override fun bindIntents() {

        val searchIntent = intent(SearchView::searchIntent)
            .switchMap { interactor.search(it) }

        val viewState = searchIntent.scan(SearchViewState.SearchNotStartedYet, interactor::reduce)
            .observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()

        subscribeViewState(viewState, SearchView::render)
    }
}