package pt.diogojg.abnassignment.ui.search.di

import dagger.Component
import pt.diogojg.abnassignment.application.di.ApplicationComponent
import pt.diogojg.abnassignment.ui.search.SearchActivity
import pt.diogojg.abnassignment.ui.search.SearchPresenter

@SearchActivityScope
@Component(dependencies = [ApplicationComponent::class], modules = [SearchActivityModule::class, SearchModule::class])
interface SearchActivityComponent {
    fun inject(activity: SearchActivity)

    fun newSearchPresenter(): SearchPresenter
}