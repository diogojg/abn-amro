package pt.diogojg.abnassignment.ui.search.di

import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.ui.search.SearchPresenter
import pt.diogojg.domain.repository.VenuesRepository
import pt.diogojg.domain.search.SearchInteractor

@Module
class SearchModule {

    @Provides
    fun providePresenter(interactor: SearchInteractor): SearchPresenter = SearchPresenter(interactor)

    @Provides
    fun provideInteractor(repository: VenuesRepository): SearchInteractor = SearchInteractor(repository)
}