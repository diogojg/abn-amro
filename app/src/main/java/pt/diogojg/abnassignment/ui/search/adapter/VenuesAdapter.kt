package pt.diogojg.abnassignment.ui.search.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pt.diogojg.domain.model.Venue

class VenuesAdapter(private val clickListener: ClickListener) : RecyclerView.Adapter<VenueViewHolder>(),
    VenueViewHolder.ClickListener {
    private val dataSet = ArrayList<Venue>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder =
        VenueViewHolder.create(parent.context, this)

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) = holder.bind(dataSet[position])

    fun setData(newData: List<Venue>) {
        val diffResult = DiffUtil.calculateDiff(ValuesDiffCallback(dataSet, newData))
        diffResult.dispatchUpdatesTo(this)
        dataSet.clear()
        dataSet.addAll(newData)
    }

    override fun onClickListener(position: Int) {
        clickListener.onCLickListener(dataSet[position])
    }

    interface ClickListener {
        fun onCLickListener(venue: Venue)
    }
}