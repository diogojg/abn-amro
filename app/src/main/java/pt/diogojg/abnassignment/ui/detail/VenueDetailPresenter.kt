package pt.diogojg.abnassignment.ui.detail

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import pt.diogojg.domain.venuedetail.VenueDetailInteractor
import pt.diogojg.domain.venuedetail.VenueDetailViewState

class VenueDetailPresenter(private val interactor: VenueDetailInteractor) :
    MviBasePresenter<VenueDetailView, VenueDetailViewState>() {

    override fun bindIntents() {
        val loadIntent = intent(VenueDetailView::loadDetailsIntent)
            .switchMap { interactor.getVenueDetails() }
            .observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()

        subscribeViewState(loadIntent, VenueDetailView::render)
    }
}