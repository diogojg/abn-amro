package pt.diogojg.abnassignment.ui.search.di

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import dagger.Module
import dagger.Provides
import pt.diogojg.abnassignment.R
import pt.diogojg.abnassignment.ui.search.Renderer
import pt.diogojg.abnassignment.ui.search.SearchActivity
import pt.diogojg.abnassignment.ui.search.adapter.SearchAdapterClickListener
import pt.diogojg.abnassignment.ui.search.adapter.VenuesAdapter

@Module
class SearchActivityModule(private val activity: SearchActivity) {

    @Provides
    fun provideRenderer(): Renderer = Renderer(activity)

    @Provides
    fun provideAdapter(clickListener: VenuesAdapter.ClickListener): VenuesAdapter =
        VenuesAdapter(clickListener)

    @Provides
    fun provideClickListener(): VenuesAdapter.ClickListener = SearchAdapterClickListener(activity)

    @Provides
    fun provideItemDecoration(): RecyclerView.ItemDecoration =
        DividerItemDecoration(activity, DividerItemDecoration.VERTICAL).apply {
            val transparentDivider = activity.getDrawable(R.drawable.divider_item_decoration)
            transparentDivider?.let { setDrawable(it) }
        }
}