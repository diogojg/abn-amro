package pt.diogojg.abnassignment.application.di

import com.squareup.picasso.Picasso
import dagger.Component
import pt.diogojg.abnassignment.application.BaseApplication
import pt.diogojg.abnassignment.data.venuesrepository.di.VenuesRepositoryModule
import pt.diogojg.domain.repository.VenuesRepository
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ContextModule::class,
        VenuesRepositoryModule::class,
        PicassoModule::class
    ]
)
interface ApplicationComponent {
    fun inject(app: BaseApplication)

    fun provideVenuesRepository(): VenuesRepository

    fun providePicasso(): Picasso
}