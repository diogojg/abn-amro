package pt.diogojg.abnassignment.application

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkRequest
import io.reactivex.plugins.RxJavaPlugins
import pt.diogojg.abnassignment.application.di.ApplicationComponent
import pt.diogojg.abnassignment.application.di.ContextModule
import pt.diogojg.abnassignment.application.di.DaggerApplicationComponent
import pt.diogojg.abnassignment.data.retrofit.internetstatus.NetworkStateCallback
import pt.diogojg.abnassignment.data.venuesrepository.di.VenuesRepositoryModule
import javax.inject.Inject


class BaseApplication : Application() {
    val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .venuesRepositoryModule(
                VenuesRepositoryModule(
                    "MDMJQ5IW2JNVTL1UMIWP0BWAEUA0IMG5PATMLGQ4M5O2SLUQ",
                    "RDZKKX04CKK44BX54AXRX5JVPHXLH3TJA40AUTANSK112S3B",
                    "https://api.foursquare.com"
                )
            )
            .contextModule(ContextModule(this))
            .build()
    }

    @Inject
    lateinit var networkStateCallback: NetworkStateCallback

    override fun onCreate() {
        super.onCreate()
        instance = this
        appComponent.inject(this)
        RxJavaPlugins.setErrorHandler { e -> } //Error handler for uncaught errors in the streams
        registerNetworkStateCallback()
    }

    private fun registerNetworkStateCallback() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.registerNetworkCallback(NetworkRequest.Builder().build(), networkStateCallback)
    }

    override fun onTerminate() {
        super.onTerminate()
        unregisterNetworkStateCallback()
    }

    private fun unregisterNetworkStateCallback() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.unregisterNetworkCallback(networkStateCallback)
    }

    companion object {
        lateinit var instance: BaseApplication
            private set
    }
}