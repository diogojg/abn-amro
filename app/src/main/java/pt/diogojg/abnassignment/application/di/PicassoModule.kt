package pt.diogojg.abnassignment.application.di

import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PicassoModule {
    @Singleton
    @Provides
    fun providePicasso(): Picasso = Picasso.get()
}