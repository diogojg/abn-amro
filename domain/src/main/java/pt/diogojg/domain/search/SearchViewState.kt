package pt.diogojg.domain.search

import pt.diogojg.domain.model.Venue

sealed class SearchViewState {
    object SearchNotStartedYet : SearchViewState()

    object LoadingFirstTime : SearchViewState()

    data class Result(val venues: List<Venue>, val isLoading: Boolean) : SearchViewState()

    data class Error(val throwable: Throwable) : SearchViewState()
}