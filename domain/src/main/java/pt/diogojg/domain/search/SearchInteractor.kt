package pt.diogojg.domain.search

import io.reactivex.Observable
import pt.diogojg.domain.repository.VenuesRepository

class SearchInteractor(private val repository: VenuesRepository) {

    fun search(query: String): Observable<PartialStateChange> = repository.getVenues(query)
        .map { PartialStateChange.Result(it) }
        .cast(PartialStateChange::class.java)
        .startWith(PartialStateChange.Loading)
        .onErrorReturn { PartialStateChange.Error(it) }

    fun reduce(oldState: SearchViewState, change: PartialStateChange): SearchViewState = when (change) {
        is PartialStateChange.Loading -> {
            if (oldState is SearchViewState.Result)
                oldState.copy(isLoading = true)
            else
                SearchViewState.LoadingFirstTime
        }
        is PartialStateChange.Result -> SearchViewState.Result(change.venues, false)
        is PartialStateChange.Error -> SearchViewState.Error(change.throwable)
    }
}