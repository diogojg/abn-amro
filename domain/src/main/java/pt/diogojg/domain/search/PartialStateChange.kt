package pt.diogojg.domain.search

import pt.diogojg.domain.model.Venue

sealed class PartialStateChange {
    object Loading : PartialStateChange()

    data class Result(val venues: List<Venue>) : PartialStateChange()

    data class Error(val throwable: Throwable) : PartialStateChange()
}