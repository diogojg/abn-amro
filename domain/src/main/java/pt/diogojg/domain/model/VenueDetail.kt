package pt.diogojg.domain.model

data class VenueDetail(
    val id: String,
    val name: String,
    val rating: Double,
    val bestPhotoUrl: String?,
    val description: String?,
    val contactInformation: ContactInformation,
    val location: Location
)