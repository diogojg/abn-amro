package pt.diogojg.domain.model

class ContactInformation(
    val facebookUsername: String?,
    val phone: String?,
    val instagram: String?,
    val twitter: String?
) {
    override fun toString(): String {
        var string = ""
        phone?.let { string += it + "\n" }
        facebookUsername?.let { string += it + "\n" }
        instagram?.let { string += it + "\n" }
        twitter?.let { string += it + "\n" }
        return string.trim()
    }
}