package pt.diogojg.domain.model

import java.io.Serializable

data class Location(
    val street: String?,
    val city: String?,
    val country: String
) : Serializable {
    override fun toString(): String = when {
        !street.isNullOrBlank() && !city.isNullOrBlank() -> String.format("%s, %s, %s", street, city, country)
        !city.isNullOrBlank() -> String.format("%s, %s", city, country)
        else -> country
    }
}