package pt.diogojg.domain.model

import java.io.Serializable

data class Venue(
    val id: String,
    val name: String,
    val location: Location
) : Serializable