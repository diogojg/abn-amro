package pt.diogojg.domain.repository

import io.reactivex.Observable
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.model.VenueDetail

interface VenuesRepository {

    fun getVenues(query: String): Observable<List<Venue>>

    fun getDetail(venue: Venue): Observable<VenueDetail>
}