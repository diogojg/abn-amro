package pt.diogojg.domain.venuedetail

import pt.diogojg.domain.model.VenueDetail

sealed class VenueDetailViewState {
    object Loading : VenueDetailViewState()

    data class Result(val venueDetail: VenueDetail) : VenueDetailViewState()

    data class Error(val throwable: Throwable) : VenueDetailViewState()
}