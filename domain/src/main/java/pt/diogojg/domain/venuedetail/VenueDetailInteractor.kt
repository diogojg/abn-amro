package pt.diogojg.domain.venuedetail

import io.reactivex.Observable
import pt.diogojg.domain.model.Venue
import pt.diogojg.domain.repository.VenuesRepository

class VenueDetailInteractor(
    private val repository: VenuesRepository,
    private val venue: Venue
) {
    fun getVenueDetails(): Observable<VenueDetailViewState> = repository.getDetail(venue)
        .map { VenueDetailViewState.Result(it) }
        .cast(VenueDetailViewState::class.java)
        .startWith(VenueDetailViewState.Loading)
        .onErrorReturn { VenueDetailViewState.Error(it) }
}